﻿using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using CloudWorks.Store.BaseClasses;

namespace CloudWorks.Store.Classes
{
    public static class FilesInteraction
    {
        public static void SavePersonsToJson(Person[] persons, string fileName)
        {
            Client[] clients = persons.Where(x => x is Client).Cast<Client>().ToArray();
            Employee[] employees = persons.Where(x => x is Employee).Cast<Employee>().ToArray();
            string jsonString = "{\"Clients\": " + JsonSerializer.Serialize(clients) + ","
                                + "\"Employees\": " + JsonSerializer.Serialize(employees) + "}";
            using (StreamWriter streamWriter = new StreamWriter(fileName))
                streamWriter.Write(jsonString);
        }

        public static Person[] LoadPersonsFromJson(string fileName)
        {
            using (StreamReader streamReader = new StreamReader(fileName))
            {
                string jsonString = streamReader.ReadToEnd();
                string[] jsonPersons = jsonString[12..^1].Split(",\"Employees\": ");
                Client[] clients = JsonSerializer.Deserialize<Client[]>(jsonPersons[0]);
                Employee[] employees = JsonSerializer.Deserialize<Employee[]>(jsonPersons[1]);
                return clients?.Cast<Person>().Concat(employees!).ToArray();
            }
        }
    }
}