﻿using CloudWorks.Store.BaseClasses;
using System;

namespace CloudWorks.Store.Classes
{
    public class Client : Person
    {
        public Client() => (Name, CheckoutSpeed) = ("Name", DefaultCheckoutSpeed);
        public Client(string name) : base(name, DefaultCheckoutSpeed) { }
        public Client(string name, double CheckoutSpeed) : base(name, CheckoutSpeed) { }

        public static double DefaultCheckoutSpeed = 1.5;

        //На даному етапі потрібні обмеження не зрозумілі, так як операції з цією колекцію не проводяться
        //Можливо в майбутньому потрібне буде сортування колекції
        //В такому разі будуть розглядатись колекції SortedList i SortedDictionary
        public Product[] Items { get; set; }

        public override void Print() => Console.WriteLine($"Client: {Name}");
    }
}
