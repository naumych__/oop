﻿using System;

namespace CloudWorks.Store.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MaxLengthAttribute: Attribute
    {
        public int MaxLength { get; set; }

        public MaxLengthAttribute() => MaxLength = 50;

        public MaxLengthAttribute(int maxLength)
        {
            if (maxLength < 1) throw new Exception("maxLength cannot be less than 1");
            MaxLength = maxLength; 
        }
    }
}