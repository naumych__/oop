﻿using System;
using System.Reflection;
using CloudWorks.Store.Interfaces;
using CloudWorks.Store.Attributes;

namespace CloudWorks.Store.BaseClasses
{
    public abstract class Person : IPrintable
    {
        protected Person() => (Name, CheckoutSpeed) = ("Name", 10);
        protected Person(string name, double checkoutSpeed)
        {
            if (name.Length > typeof(Person).GetProperty("Name")?.GetCustomAttribute<MaxLengthAttribute>()?.MaxLength)
                throw new Exception("A Person's name cannot be longer than allowed");
            Name = name;
            CheckoutSpeed = checkoutSpeed;
        }
        [MaxLength(20)]
        public string Name { get; set; }

        public double CheckoutSpeed { get; set; }
        public abstract void Print();
    }
}
