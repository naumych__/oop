﻿using CloudWorks.Store.BaseClasses;
using CloudWorks.Store.Classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CloudWorks.Store
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode; 
            Console.InputEncoding = System.Text.Encoding.Unicode;
            
            TestSaveAndLoadPersonsFromFile();
            TestReflectionVsDirectCall();
            
            int seldCheckoutCount = 0;
            while (seldCheckoutCount == 0)
            {
                Console.WriteLine("Введіть загальну кількість кас самообслуговування в магазині");
                int.TryParse(Console.ReadLine(), out seldCheckoutCount);
            }

            /* Filling Persons */

            Person[] persons;
            persons = FillPersons();
            
            /*******************/

            var store = new Classes.Store(seldCheckoutCount, persons);

            foreach (var person in persons)
            {
                person.Print();
            }

            store.Print();

            Console.WriteLine($"Загальний час обслуговування: {store.Checkout()}");
        }

        private static void TestSaveAndLoadPersonsFromFile()
        {
            List<Person> persons = new List<Person>();
            for (int i = 0; i < 10; i++)
            {
                persons.Add(new Client("Client" + i, i)
                {
                    Items = new Product[]
                    {
                        new Product(){Name = "Product" + i},
                        new Product(){Name = "Product" + i},
                        new Product(){Name = "Product" + i}
                    }
                });
                persons.Add(new Employee("Employee" + i, i));
            }
            FilesInteraction.SavePersonsToJson(persons.ToArray(), "file1.json");
            Person[] persons2 = FilesInteraction.LoadPersonsFromJson("file1.json");
            
            Console.WriteLine();
            foreach (var itemPerson in persons2)
            {
                Console.Write($"{itemPerson.Name}: {itemPerson.CheckoutSpeed}");
                if (itemPerson is Client client)
                    Console.Write($", Items: {String.Join(", ", client.Items.Select(x => x.Name))} \n");
                else
                    Console.WriteLine();
            }
        }
        
        private static void TestReflectionVsDirectCall()
        {
            List<Person> persons = new List<Person>();
            for (int i = 0; i < 50; i++)
            {
                persons.Add(new Client("Name" + i));
                persons.Add(new Employee("Name" + i));
            }
            var store = new Classes.Store(20, persons.ToArray());
            List<Person> result1 = new List<Person>();
            List<Person> result2 = new List<Person>();

            TimeSpan reflectionTime;
            TimeSpan directlyCallTime;
            Stopwatch stopwatch = new Stopwatch();
            
            
            stopwatch.Start();
            for (int i = 0; i < 1000; i++)
            {
                result1 = store.GetType().GetMethod("GetPersons")?.Invoke(store, null) as List<Person> ?? persons;
            }
            stopwatch.Stop();
            reflectionTime = stopwatch.Elapsed;
            
            
            stopwatch.Restart();
            for (int i = 0; i < 1000; i++)
            {
                result2 = store.GetPersons();
            }
            stopwatch.Stop();
            directlyCallTime = stopwatch.Elapsed;
            
            
            Console.WriteLine($"Время рефлексивных вызовов: {reflectionTime.Milliseconds} milliseconds");
            Console.WriteLine($"Время прямых вызовов: {directlyCallTime.Milliseconds} milliseconds");
        }
        private static Person[] FillPersons()
        {
            int personsCount = 0;
            while (personsCount == 0)
            {
                Console.WriteLine("Введіть загальну кількість людей в магазині");
                int.TryParse(Console.ReadLine(), out personsCount);
            }
            var persons = new Person[personsCount];

            for (var i = 0; i < personsCount; i++)
            {
                Console.WriteLine("Введіть ім'я");
                var name = Console.ReadLine();

                Console.WriteLine("Якщо ви хочете додати працівника введіть \"1\". Якщо ви хочете додати покупця введіть \"2\"");
                var personType = Console.ReadLine();

                if (personType == "1")
                {
                    Console.WriteLine($"Швидкість продажу працівником відрізняється від {Employee.DefaultCheckoutSpeed}? т(так)/н(ні)");
                    var isCheckoutSpeedDifferent = Console.ReadLine();
                    if (isCheckoutSpeedDifferent == "т")
                    {
                        Console.WriteLine("Введіть швидкість продажу співробітником");
                        var speedStr = Console.ReadLine();
                        if (double.TryParse(speedStr, out var speed))
                        {
                            persons[i] = new Employee(name, speed);
                        }
                        else
                        {
                            Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                            i--;
                            continue;
                        }
                    }
                    else if (isCheckoutSpeedDifferent == "н")
                    {
                        persons[i] = new Employee(name);
                    }
                    else
                    {
                        Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                        i--;
                        continue;
                    }
                }
                else if (personType == "2")
                {
                    Console.WriteLine($"Швидкість покупки клієнтом відрізняється від {Client.DefaultCheckoutSpeed}? т(так)/н(ні)");
                    var isCheckoutSpeedDifferent = Console.ReadLine();
                    if (isCheckoutSpeedDifferent == "т")
                    {
                        Console.WriteLine("Введіть швидкість покупки клієнтом");
                        var speedStr = Console.ReadLine();
                        if (double.TryParse(speedStr, out var speed))
                        {
                            var client = new Client(name, speed);
                            client.Items = ReadProductList().ToArray();
                            persons[i] = client;
                        }
                        else
                        {
                            Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                            i--;
                            continue;
                        }
                    }
                    else if (isCheckoutSpeedDifferent == "н")
                    {
                        var client = new Client(name);
                        client.Items = ReadProductList().ToArray();
                        persons[i] = client;
                    }
                    else
                    {
                        Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                        i--;
                        continue;
                    }
                }
                else
                {
                    Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                    i--;
                    continue;
                }
            }
            
            return persons;
        }

        public static IEnumerable<Product> ReadProductList()
        {
            var choise = "т";
            while (choise == "т")
            {
                Console.WriteLine("Введіть назву продукта");
                yield return new Product { Name = Console.ReadLine() };
                Console.WriteLine("Продовжити заповнення корзини? т(так)");
                choise = Console.ReadLine();
            }
        }
    }
}
